\documentclass{article}

%\usepackage[margin=0.6in]{geometry}
\usepackage[a4paper,%
            left=0.5in,right=0.5in,top=0.45in,bottom=0.5in,%
            footskip=.25in]{geometry}
    \usepackage[parfill]{parskip}
    \usepackage[utf8]{inputenc}
    \usepackage{amsmath,amssymb,amsfonts,amsthm}
    \usepackage{xcolor}
    \usepackage{setspace}\setstretch{1,4}
    \usepackage{fancyhdr}
    \usepackage{graphicx}
    \usepackage{bold-extra}
    \usepackage{comment}
 %   \pagestyle{fancy}
    \renewcommand\headrulewidth{0pt}
    \renewcommand\footrulewidth{1pt}
%    \fancyfoot[R]{pierre.wulles@etu.univ-grenoble-alpes.fr}
    \definecolor{gris}{gray}{0.85}
    \newcommand{\surl}[1]{\colorbox{gris}{\textbf{#1}}}
    \newcommand{\dx}{\mathrm{d}x}
    \newcommand{\dt}{\mathrm{d}t}
    \newcommand\quadri[1]{%
      \medbreak\textcolor{gray}
                         {\setlength\unitlength{5mm}
                           \begin{picture}(34,#1)
                             \multiput(0,0)(1,0){35}{\line(0,1){#1}}
                             \put(0,0){\line(1,0){34}}
                             \multiput(0,1)(0,1){#1}{\line(1,0){34}}
                             \end{picture}}\smallbreak}


    \newcommand\defi{\bfseries{\scshape{Définition \;}}}
    \newcommand\encadre[1]{\fbox{\begin{minipage}{0.96\linewidth}#1\end{minipage}}}
    \newcommand\ligne{\begin{center}\line(1,0){250}\end{center}}
    \newenvironment{solution}{\begin{center}\begin{itshape}}{\end{itshape}\end{center}}
    \newenvironment{question}{\ignorespaces}{}

%        \newtheorem{question}{}

% pour ne pas afficher les solutions
        %\excludecomment{solution}
        %\excludecomment{question}
\begin{document}

\begin{center}
  \Huge
  \fbox{\fbox{
      \textbf{Forces et mouvements}}}
\end{center}
\section{Mouvements uniformes}
\subsection{Vitesse d'un corps}
L'étude du mouvement est un des objectifs \surl{fondamentaux} de la
physique. Dans notre monde, tout bouge: les planètes, les étoiles, les
oiseaux, les pommes qui tombent ... Très tôt les physiciens se sont
intéressés à l'étude du mouvement. \\ On s'aperçoit facilement que
\surl{temps} et \surl{espace} sont liés: il faut un certain temps pour
aller d'un point $A$ à un point $B$, \textbf{aucun déplacement n'est
  instantané.} \\ Mais comment faire le lien ?
\[ \includegraphics[scale=0.8]{vitesse.png} \]
Si l'on suppose que le véhicule met autant de temps pour aller de 0m à 10 m que pour aller de 10m à 20m. Alors la distance parcourue est proportionelle au temps ! Si le véhicule roule pendant deux fois plus longtemps: il parcourt deux fois plus de distance !
On établi donc la relation de proportionalité suivante:\\[1mm]
\encadre{
  \[ \qquad \]}\\[1mm]
%d = v \times t
Où le coefficient de proportionalité $v$ est communément appelé: \surl{vitesse moyenne} qui a pour unité m/s.\\
Dans cette situation idéale, $v$ est toujours la même. On dit alord de la trajectoire qu'elle est \surl{rectiligne uniforme}. \\
Dans certains cas (un manège par exemple), la vitesse est constante mais le mouvement n'est pas droit:
\[\includegraphics[scale=0.6]{circulaire.png}\]
One parle alors de \surl{trajectoire circulaire uniforme}
\subsection{Notion de vecteur}
Un \surl{vecteur} est un object mathématique qui permet de regrouper en un seul élément plusieurs informations caractérisant un système physique (comme une voiture en déplacement) ou bien une notion plus abstraite. Il n'y a rien de compliqué là dedans, il faut juste savoir que quand on manipule un vecteur, on manipule en même temps:
\begin{itemize}
\item Un nombre
\item Une direction
\item Un sens
\end{itemize}
On note les vecteurs avec une flèche au dessus de la lettre: $\vec{v}$\\
Le nombre dont il est question est appelé \surl{norme} et on notera $||\vec{v}|| = v$\\
On définit l'opération \surl{produit scalaire} entre deux vecteurs comme le produit des normes fois le cosinus de l'angle considéré:\\
\encadre{
\[ \vec{u}\cdot\vec{v} = ||u|| \times ||v|| \times \cos(\theta) \]}\\[1mm]
Cette formule n'est pas à connaître, il faut cependant garder en tête que le produit scalaire dépend des normes et de l'angle et que en particulier:
\begin{center}
\surl{Le produit scalaire de deux vecteurs orthogonaux est nul.}
\end{center}
\[\includegraphics[scale=0.5]{produit_scalaire.png}\]
\section{Mouvements accelérés}
Souvent la vitesse n'est pas constante au cours du mouvement. Par exemple une voiture à l'arrêt doit voir sa vitesse augmenter pour arriver à 100km/h, l'augmentation de vitesse s'appelle \surl{accélération}, la diminution de vitesse s'appelle \surl{déccélération}, on peut aussi parfois parler d'accélération négative.\\
Si la voiture est immobile ou roule à vitesse constante alors le fil est vertical:
\[\includegraphics[scale=0.4]{acc1.png}\]
Si par contre la voiture subit une \surl{accélération} le fil s'incline vers l'arrière:
\[\includegraphics[scale=0.4]{acc2.png}\]
À l'inverse en cas de \surl{déccélération} le fil s'incline vers l'avant.
\[\includegraphics[scale=0.4]{acc3.png}\]
Pour calculer l'accélération d'un corps dans le temps on calcule le rapport suivant:\\
\encadre{
  \[ a = \frac{v_{\text{finale}} - v_{\text{initiale}}}{t_{\text{finale}} - t_{\text{initiale}}} = \frac{\Delta v}{\Delta t} \]}\\
$a$ est alors l'accélération qui peut être négative, l'unité est le m/s$^2$.\\
\textbf{L'accélération est à la vitesse ce que la vitesse est à la position.} \\ Dans ce cours nous n'étudierons que des cas ou l'accélération est constante, on parle alors de
\begin{center}
  \surl{mouvement uniformément accéléré}.
\end{center}
Supposons donc que notre voiture subit une accélération uniforme $a$ pendant une durée $t$. L'accélération fait augmenter la vitesse pendant la durée $t$  si bien qu'à la fin on a une vitesse égale à $at$. On peut donc calculer la vitesse moyenne comme suit:
\[ v_{\text{moyenne}} = \frac{v_{\text{finale}}-v_{\text{initiale}}}{2} = \frac{at - 0}{2} = \frac{at}{2} \]
Et de là on peut déduire la longueur $L$ de segment parcouru en multipliant par le temps selon la relation $d=vt$:\\[1mm]
\encadre{
  \[ L = \frac{1}{2}at\times t = \frac{1}{2}at^2 \]}\\[1mm]
Prenons donc un instant pour voir le chemin parcouru: nous sommes maintenant capable de calculer la \textbf{distance parcourue} par un objet \textbf{uniformément accéléré} pendant un temps $t$.
\section{Forces et actions mécaniques}
\subsection{Le champ de pesanteur}
Galilée s'est beaucoup interessé à ce problème. En effet selon cette théorie tous les objets, quelquesoient leurs masses devraient tomber à la même vitesse. Or il est facile de faire cette expérience et de voir que c'est faux ! Cela provient de la résistance de l'air, un phénomène que nous n'avons pas pris en compte ici.
\[ \includegraphics[scale=0.5]{chute_lune.png}\]
En négligeant la résistance de l'air (masse importante, forme sphérique) Galilée avait obtenu le résultat suivant:
\[ \includegraphics[scale=0.6]{chute.png}\]
Cela permet de calculer l'accélération du \surl{champ de pesanteur} sur Terre:\\
\encadre{:\\[10mm]}\\
Il s'agit d'une constante fondamentale de la physique !
\subsection{La première loi et le principe d'inertie}
Pour Aristote les mouvements des choses étaient liés à leurs natures. La Lune tourne autour de la Terre parceque c'est dans sa nature. La pierre tombe parcequ'il est dans sa nature d'être attiré vers le bas. Si ces explications peuvent aujourd'hui sembler absurde au regard des connaissances modernes, il ne faut pas oublier qu'à l'époque d'Aristote les sciences en étaient à leur début et qu'Aristote en tentant d'expliquer le monde a été le premier à avoir une démarche de naturaliste.
\[ \includegraphics[scale=0.3]{aristote.png}\]
Galilée bouleverse alors les croyances de son époque en réalisant qu'un objet n'a pas besoin de force pour conserver en \surl{mouvement rectiligne uniforme}.\\
Imaginons que l'on pousse un livre sur une table que se passe t-il ?\\ Le livre bouge puis ralentit jusqu'à s'immobiliser à cause des frottements. Imaginons que l'on ait une table beaucoup plus polie, alors le livre ira plus loin. Et si la table est parfaitement polie et qu'il n'y a aucun frottements, que va t-il se passer ? Le livre va avancer indéfiniment !\\
En réalisant cela Galilée réalise qu'un solide, en l'absence d'interaction avec l'extérieur, est en \surl{MRU}. Un solide au repos ... peut donc bouger !\\ Cependant Galilée ne donne pas une formulation complète de cette idée et c'est donc à Newton que l'on doit le principe d'inertie connue sous sa formulation moderne:\\[2mm]
\encadre{\fbox{Première Loi de Newton:}
  \begin{center}\surl{ En l'absence de force extérieur, un solide est en mouvement rectiligne uniforme.}
\end{center}}\\
Cela permet ainsi de donner une définition de force comme \textit{phénomène qui modifie l'inertie d'un objet}.
\subsection{La deuxième loi}
Intuitivement Newton comprend que pour modifier l'inertie d'un solide, pour le mettre en mouvement il faut fournir une force et plus cette force sera grande plus le solide bougera, et de façon inverse plus ce solide est massif plus il sera difficile de le déplacer. 

\end{document}
