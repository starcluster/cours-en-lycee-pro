import matplotlib.pyplot as plt
from statistics import *

nicolas = [12 , 7 , 19 , 10 , 6 , 3 , 7 , 16 , 12 , 12 , 13 , 17]
mathilde =[7 , 7 , 20 , 9 , 8 , 2 , 5 , 9 , 10 , 11 , 11 , 8]                                      
maxime = [17 , 16 , 10 , 19 , 15 , 14 , 19 , 12 , 10 , 18 , 17 , 15]

print(sum(nicolas)/len(nicolas))
print(sum(mathilde)/len(mathilde))
print(sum(maxime)/len(maxime))
nicolas.sort()
maxime.sort()
mathilde.sort()
print(nicolas[5])
print(mathilde[5])
print(maxime[5])
print(nicolas[11]-nicolas[0])
print(mathilde[11]-mathilde[0])
print(maxime[11]-maxime[0])
print(nicolas[2],nicolas[8])
print(mathilde[2], mathilde[8])
print(maxime[2], maxime[8])


fig = plt.figure(1, figsize=(7, 6))

plt.subplot(311)
plt.hist(nicolas, range = (0, 20), bins = 4, color = 'yellow',
            edgecolor = 'black')
plt.ylabel("Nicolas")
plt.subplot(312)
plt.hist(mathilde, range = (0, 20), bins = 4, color = 'yellow',
            edgecolor = 'black')
plt.ylabel("Mathilde")
plt.subplot(313)
plt.hist(maxime, range = (0, 20), bins = 4, color = 'yellow',
            edgecolor = 'black')
plt.ylabel("Maxime")
#plt.show()
 
