#+TITLE:     Fluctuations statistiques
#+AUTHOR:    M. Wulles
#+EMAIL:     
#+DATE:      Novembre 2019
#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  fr
#+OPTIONS:   H:3 num:nil toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME: 
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+BEAMER_FRAME_LEVEL: 0
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+latex_header:\addtolength{\headsep}{1cm}
* 
** 
[[file:voiture1.png]]
\newpage
** 
[[file:voiture3.png]]
\newpage
** 
- Combien de voitures rouges ? 
\newpage
** 
| Voiture | Effectif | Fréquence |
|---------+----------+-----------|
| Rouge   |        9 |           |
| Vert    |        4 |           |
| Bleu    |        3 |           |
| Total   |       16 |         1 |

\newpage
** 
| Voiture | Effectif | Fréquence         |
|---------+----------+-------------------|
| Rouge   |        9 | 9/16 \approx 0.56 |
| Vert    |        4 | 4/16 \approx 0.25 |
| Bleu    |        3 | 3/16 \approx 0.19 |
| Total   |       16 | 1                 |
\newpage
** 
[[file:voiture4.png]]
\newpage

** 
[[file:voiture5.png]]
\newpage
** 
[[file:voiture6.png]]
\newpage
** 
[[file:voiture7.png]]
\newpage
** 
[[file:voiture8.png]]
\newpage
** 
[[file:voiture9.png]]
\newpage
** 
[[file:voiture10.png]]
\newpage
** 
| Voiture | Effectif | Fréquence         |
|---------+----------+-------------------|
| Rouge   |        8 | 8/16 = 0.5        |
| Vert    |        3 | 3/16 \approx 0.19 |
| Bleu    |        5 | 5/16 \approx 0.31 |
| Total   |       16 | 1                 |
\newpage
** 
[[file:fluct1.png]]
\newpage
** 
[[file:fluct2.png]]
\newpage
** 
[[file:fluct3.png]]
\newpage
** En résumé
On note $p$ la fréquence d'un caractère dans une population.
#+latex:\\[6mm]
On note $f$ la fréquence du caractère dans un échantillon de taille $n$.
#+latex:\\[6mm]
Alors pour 95% des échantillons:
\[ f \in \left[ p - \frac{1}{\sqrt{n}} ; p + \frac{1}{\sqrt{n}} \right] \]
C'est *l'intervalle de fluctuation*.

Pour avoir une idée de $p$ on peut calculer *la moyenne de la fréquence des échantillons*:
\[ f = \frac{f_1 + f_2 + ... + f_k}{k} \]
