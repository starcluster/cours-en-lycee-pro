import matplotlib.pyplot as plt
import random

N = 30

def avoir6():
    j = 0
    for i in range(100):    
        if random.randint(2,6) == 6:
            j = j + 1
    return j

x = [i for i in range(N)]
y = [avoir6() for i in range(N)]

plt.plot(x,y, '.', markersize=20)
plt.ylim(0,50)
plt.show()
