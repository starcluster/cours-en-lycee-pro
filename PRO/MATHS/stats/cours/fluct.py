from random import *
import matplotlib.pyplot as plt
from math import sqrt
import seaborn
seaborn.set()
N = 400
voitures = ['rouge']*600+[ 'bleu']*200+200*[ 'vert']
x = [i for i in range(N)]
y = []
lim_bas = [0.5]*N
lim_haut = [0.7]*N
for i in range(N):
    shuffle(voitures)
    k = 2
    rouge = voitures[:80+i*k].count('rouge')
    bleu = voitures[:80+i*k].count('bleu')
    vert = voitures[:80+i*k].count('vert')
    y.append(rouge/(80+i*k))
    print(rouge, vert, bleu)

plt.ylim(0.4,0.8)
print(lim_bas)
plt.xlabel("Tirage dans un échantillon de plus en plus grand")
plt.ylabel("Nombre de voitures rouges")
plt.plot(x,y, '+')
#plt.plot(x,lim_bas)
#plt.plot(x,lim_haut)
plt.show()
#print(voitures)
