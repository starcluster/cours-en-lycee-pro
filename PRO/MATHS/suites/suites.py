import matplotlib.pyplot as plt
import numpy as np

N = 10

r = 4
u0 = 1

q = 2
v0 = 1

n = [i for i in range(N)]
u = [r*i*10 + u0 for i in range(N)]
v = [r**i * u0 for i in range(N)]


#plt.subplot('121')
plt.plot(n,u, '.', markersize=16)
plt.title("Comparaison")
#plt.subplot('122')
plt.plot(n,v, '.', markersize=16)
#plt.title("Suite décroissante")
plt.show()
