#+TITLE:     Fonctions
#+AUTHOR:    M. Wulles
#+EMAIL:     
#+DATE:      Février 2019
#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  fr
#+OPTIONS:   H:3 num:nil toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME: 
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+BEAMER_FRAME_LEVEL: 0
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+latex_header:\addtolength{\headsep}{1cm}
#+latex_header: \usepackage{tikz,tkz-tab}
* 
** 
Un chaudronnier souhaite découper une tôle, il peut soit le faire à la main à l'aide d'une meuleuse, soit utiliser une machine à découpe plasma.
[[file:machine.png]]
\newpage
** 
[[file:machinefx.png]]
\newpage
** 
Si \fbox{$y = f(x)$}
**** $f$ est une *fonction*
**** $y$ est *l'image* de $x$ par la fonction $f$ 
**** $x$ est *l'antécédent* de $y$ par rapport à la fonction $f$
**** $x$ est une *variable* de $f$
# **** La *représentation graphique* d'une fonction est une façon simple 
\newpage
** 
Exemple: si $f(x) = 5x$:
**** Calculer l'image de 3 par $f$
**** Calculer l'antécédent de 50 par $f$
\newpage
** 
Comment se repérer dans un plan ? 
[[file:map.jpg]]
\newpage
** 
En mathématiques on utilisera un *repère*:
[[file:repere.png]]
\newpage
[[file:repere.png]]
**** Placer le point d'abscisse 1 et d'ordonnée 3
**** Placer le point (-2;-3)
\newpage
** 
De cette façon on peut représenter une fonction affine par exemple:
[[file:f1.png]]
\newpage
** 
Il en existe bien d'autres (Polynômes): 
[[file:f2.png]]
\newpage
** 
[[file:f3.png]]
\newpage
**** Quelle est l'image de 4 par cette fonction ?
**** Quelle est l'antécédent de 10 par cette fonction ?
\newpage
** 
Utilisation du *tableau de variation*: un moyen compact de représenter une fonction !
\begin{center}
\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , $f(x)$ / 1}{$-16$, $-1$,$1$, $16$}
\end{tikzpicture}
 \end{center}
[[file:f4.png]]
\newpage
